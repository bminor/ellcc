#!/bin/sh
# ELLCC GNU tools build script.

# Get the staging directory.
prefix=`cd ..; pwd`

# Figure out the compilers to use.
. ../build-setup $*

echo Building the GNU tools with:
echo C compiler: $cc
echo C++ compiler: $cxx
echo In: llvm-build$builddir

# Configure for GNU tools.
mkdir -p gnu-build$builddir

# Configure for binutils.
# binutils are build to support all targets in the target list.
cd gnu-build$builddir
packages="binutils"
for p in $packages; do
  echo Configuring package $p for $targets
  mkdir $p
  cd $p
  ../../src/$p/configure CC="$cc" CXX="$cxx" AR="$ar" --enable-64-bit-bfd \
    --bindir=$bindir --host=$host-$os --enable-targets=$targetlist \
    --program-prefix=ecc- --prefix=$prefix/lib \
    --datadir=$prefix/lib/share
  if [ $? -ne 0 ] ; then
    echo configure for $p failed
    exit 1
  fi

  sed -e "s:/.*/gnu/src/missing ::g" Makefile > tmp$$
  mv tmp$$ Makefile
  # We have to make binutils here so the configuration of the assemblers
  # later in this file works. Ugh!
  make || exit 1
  cd ..
done
cd ..

# Configure for assemblers for all targets.
cd gnu-build$builddir
cd binutils
for t in $targets; do
  mkdir $t
  cd $t
  echo Configuring the assembler for $t
  ../../../src/binutils/gas/configure CC="$cc" CXX="$cxx" AR="$ar" \
      --bindir=$bindir --target=$t --program-prefix=${t}- \
      --host=$host-$os --prefix=$prefix/lib --datadir=$prefix/lib/share
  if [ $? -ne 0 ] ; then
    echo configure for $t failed
    exit 1
  fi
  make || exit 1
  cd ..
done
cd ../..

if [ "$host" != "$build" ] ; then
  # The host system is not the build system.
  # Build only  binutils and the assemblers.
  # Finally, install into the target specific bin dir.
  mkdir -p $bindir
  make TARGETDIR=$builddir BINDIR=$bindir installcross || exit 1
  exit 0
fi

# Configure for GDB.
# GDB is built to support all GDB targets.
cd gnu-build$builddir
packages="gdb"
for p in $packages; do
  echo Configuring package $p for $targets
  mkdir $p
  cd $p
  ../../src/$p/configure CC="$hcc" CXX="$hcxx" --enable-64-bit-bfd \
    --enable-targets=$targetlist \
    --program-prefix=ecc- --prefix=$prefix/lib --bindir=$bindir \
    --datadir=$prefix/lib/share
  if [ $? -ne 0 ] ; then
    echo configure for $p failed
    exit 1
  fi

  sed -e "s:/.*/gnu/src/missing ::g" Makefile > tmp$$
  mv tmp$$ Makefile
  make || exit 1
  cd ..
done
cd ..

# Configure for QEMU.
cd src/qemu
# The QEMU system targets.
qemu_target_list="i386-softmmu x86_64-softmmu arm-softmmu \
    microblaze-softmmu mips-softmmu mipsel-softmmu \
    ppc-softmmu ppc64-softmmu sparc-softmmu"

if [ `uname` = "Linux" ] ; then 
    # The QEMU Linux user space targets.
    qemu_target_list="$qemu_target_list i386-linux-user \
    x86_64-linux-user arm-linux-user armeb-linux-user \
    microblaze-linux-user mips-linux-user mipsel-linux-user \
    ppc-linux-user ppc64-linux-user ppc64abi32-linux-user sparc-linux-user"
fi

echo Configuring package qemu for $targets
./configure --cc="$hcc" --host-cc="$hcc" --prefix=$prefix --disable-strip \
  --target-list="$qemu_target_list" --disable-guest-agent
if [ $? -ne 0 ] ; then
  echo configure for qemu failed
  exit 1
fi
make
cd ../..

# Finally, install everything.
make TARGETDIR=$builddir install || exit 1
