TARGET := ppc64
Arch.$(TARGET) := ppc64
TARGET.$(TARGET) := -target $(TARGET)-ellcc-$(OS)
CFLAGS.$(TARGET) := $(TARGET.$(TARGET)) $(CFLAGS)
CXXFLAGS.$(TARGET) := $(TARGET.$(TARGET)) $(CXXFLAGS)
LDFLAGS := $(TARGET.$(TARGET))
